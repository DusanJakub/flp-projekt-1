%Example of random Connect6 player
%reading stdin and writing stdout.
%(c) S. Zidek 2013

%"Compilation":
%	 swipl -q -g prolog -o xdumbo00 -c c6-dumb.pl

%CHANGELOG:
%v1: removed gtrace & starting whole game, i.e. with FIRST/START message
%v2: fixed error in get_coords - Sy2 contained semicolon
%v3: uncommented entry point
%v4: fixed asserting first stone
%v5: adapted to new SWI Prolog

:- consult(rules).
:- consult(alphabeta).

:- dynamic endTime/1.
timeout(T):-
	endTime(OLD),
	retract(endTime(OLD));
	get_time(T1),
	T2 is T1 + T,
	assert(endTime(T2)).

%Reads line from stdin, terminates on LF or EOF.
read_line(L) :-
	get_char(C),
	(isEOFEOL(C), L = [], !;
		read_line(LL), atom_codes(C,[Cd]),
		[Cd|LL] = L).

%Tests if character is EOF or LF.
isEOFEOL(C) :-
	C == end_of_file, halt; 
	(char_code(C,Code), Code==10).


get_coords1(L, X, Y) :-
	atom_codes(';', [SemiColon]),
	bagof(Isc, nth0(Isc, L, SemiColon), [Isc]),
	string_to_list(S, L),

	atom_codes(',', [Comma]),
	bagof(Ic, nth0(Ic, L, Comma), [Icom]),
	sub_string(S, 0, Icom, _, Sx), 
	Sybeg is Icom+1,
	Sylen is Isc-Icom-1,
	sub_string(S, Sybeg, Sylen, _, Sy),

	string_to_list(Sx, Cx),
	string_to_list(Sy, Cy),

	number_codes(X, Cx),
	number_codes(Y, Cy).

get_coords(L, X1, Y1, X2, Y2) :-
	atom_codes(';', [SC]),
	bagof(Isc, nth0(Isc, L, SC), SemiColons),
	SemiColons = [SC1, _SC2],
	string_to_list(S, L),

	sub_string(S, 0, SC1, _, S1),
	string_to_list(S1, L1),
	atom_codes(',', [Comma]),
	bagof(Ic1, nth0(Ic1, L1, Comma), [Icom1]),
	sub_string(S1, 0, Icom1, _, Sx1), 
	Sy1beg is Icom1+1,
	sub_string(S1, Sy1beg, _, 0, Sy1),

	S2beg is SC1+1,
	sub_string(S, S2beg, _, 0, S2),
	string_to_list(S2, L2),
	bagof(Ic2, nth0(Ic2, L2, Comma), [Icom2]),
	sub_string(S2, 0, Icom2, _, Sx2), 
	Sy2beg is Icom2+1,
	sub_string(S2, Sy2beg, _, 1, Sy2),

	string_to_list(Sx1, Cx1),
	string_to_list(Sy1, Cy1),
	string_to_list(Sx2, Cx2),
	string_to_list(Sy2, Cy2),

	number_codes(X1, Cx1),
	number_codes(Y1, Cy1),
	number_codes(X2, Cx2),
	number_codes(Y2, Cy2).

write_stones(X1, Y1, X2, Y2) :-
	number_codes(X1, Sx1),
	number_codes(Y1, Sy1),
	number_codes(X2, Sx2),
	number_codes(Y2, Sy2),

	append(["STONES:", Sx1, ",", Sy1, ";", Sx2, ",", Sy2, ";"], L),
	put_line(L).

gen_pos(S, X, Y) :-
	X is random(S) + 1,
	Y is random(S) + 1.

gen_random_free(X, Y) :-
	tableSize(S),
	%TODO: check if board is full
	repeat,
	gen_pos(S, X, Y),
	\+ stone(X, Y, _, _).

move1(X,Y,[(X,Y)|_]) :-
	playStone(X,Y,black), !.

move1(X,Y,[]) :-
	gen_random_free(X, Y),
	playStone(X,Y,black).
	
move(X1, Y1, X2, Y2) :-
	alphabeta(M1),
	M1 = [_|M2],
	move1(X1,Y1,M1), move1(X2,Y2,M2),
	!.

move(_,_,_,_) :-
	writef("Alphabeta failed\n"),
	fail.

put_line(L) :-
	writef('%s\n', [L]).
	
start :-
	initWindowPositions,
	read_line(L),
	(
		atom_codes('FIRST:', AC),
		append(AC, CS, L),
		get_coords1(CS, X, Y),
		playStone(X,Y,white),
		move(X1, Y1, X2, Y2),
		write_stones(X1, Y1, X2, Y2),
		playStone(X1,Y1,white),
		playStone(X2,Y2,white)
	;
		L = "START;",
		tableSize(S),
		M is (S+1)//2,
		playStone(M,M,black),
		writef('FIRST:%w,%w;\n', [M,M])
	;
		halt
	),
	play.
		
play :-
	read_line(L),
	(
		atom_codes('QUIT;', AC),
		L == AC, halt
	;
		atom_codes('STONES:', AC),
		append(AC, CS, L),
		get_coords(CS, X1o, Y1o, X2o, Y2o),
		playStone(X1o,Y1o,white),
		playStone(X2o,Y2o,white),
		move(X1, Y1, X2, Y2),
		write_stones(X1, Y1, X2, Y2),
		play
	;
		halt
	).

%entry point
prolog :-
	prompt(_, ''),
	start.





%% tento soubor muzete ve svých projektech libovolnì pouzít
%% PS: minulý rok bylo jedním studentem vytvoøeno grafické rozhraní,
%%     ke stazení na https://gist.github.com/izidormatusov/5114798
%% PS2: zejména pøi pouzívání rùzných knihoven si dávejte dobrý pozor,
%%     zda je pozadovaná funkce dostupná na referenèním serveru
%% M. Hyrs, 21.3.2014
