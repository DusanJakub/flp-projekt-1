#!/usr/bin/env bash

LOCAL_OUT_PATH="."  
LOG_PATH="."

ghc main.hs

# comments for tests
declare -A arr
arr[tests_F_assignment_pas]="Pokus o prirazeni bez ':' (:=)"
arr[tests_F_undeclared_var_pas]="Zapis do nedeklarovane promenne"
arr[tests_F_wrong_type_assignment_pas]="Prirazeni x:integer = 'string'"
arr[tests_F_wrong_type_assignment_2_pas]="Prirazeni x:string = 1"
arr[tests_T_assignment_2_pas]="Prirazeni x:string = '15'"
arr[tests_test_pas]="Cteni int, string, double"
arr[tests_T_fact1_iter_pas]="Faktorial iterativne"
arr[tests_T_fact2_rec_pas]="Faktorial rekurzivne"
arr[tests_operators_pas]="Testovani funkce DIV"
arr[tests_operator_priority_pas]="Test priority operatoru"
arr[tests_xtest_pas]="Pokusny soubor"
arr[tests_functions_1_pas]="Pretezovani promennych ve fcich"

echo " "

for file in tests/*
do
    echo "Running test $file:"
    echo "====================================================="
    index=""

    index=`echo $file | sed 's/\//_/'`
    index=`echo ${index/./_}`
    ind=${arr[$index]}
    size=$((33-${#ind}))
    size2=$(($size/2))
    size4=$size2
    size3=$((size2*2))
    
    if [ $size3 -ne $size ]
    then
      size4=$(($size4+1))
    fi
    
    echo -ne "========= "
    eval "printf ' '%.0s {1..$size2}"
    echo -ne ${arr[$index]}
    eval "printf ' '%.0s {1..$size4}"
    echo " ========="
    echo "====================================================="
    ./main $file
    echo "====================================================="
    echo " "
    echo " "
done