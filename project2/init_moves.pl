:- dynamic firstStone/2, myMove/3, moveCnt/1.

% ========== databaze tahu ==========
assert(myMove(0,11,11)).

% ============= nezacinam =============
% sloupec stejny, radek -1
assert(myMove(1,-1, 0)).
assert(myMove(2, 0, 1)).

% zakladni tah, prodlouzeni uhlopricky dolu
assert(myMove( 3, 1, 2)).
assert(myMove( 4, 2, 3)).

% nahradni tah, prodlouzeni uhlopricky nahoru
assert(myMove(32,-2,-1)).
assert(myMove(42,-3,-2)).

% vertikalne
assert(myMove(33,-1,-1)).
assert(myMove(43, 1, 1)).

% ============= zacinam =============
% prvni uhlopricka +-1
assert(myMove( 5,-1,-1)).
assert(myMove( 6, 1, 1)).

% druha uhlopricka +-1
assert(myMove(52,-1, 1)).
assert(myMove(62, 1,-1)).

% vertikalne +-1
assert(myMove(53,-1, 0)).
assert(myMove(63, 1, 0)).

% horizontalne +-1
assert(myMove(54, 0, 1)).
assert(myMove(64, 0,-1)).

% ============= end =============


assert(moveCnt(1)).




% ========== provedeni jednotlivych tahu ==========

/**
 * pokracuji 3. a 4. pultahem (moveCnt je 3)
 * nezacinal jsem
 * overim, ze jsou pole volna a polozim kameny
 */

 % navazujici uhlopricka smerem dolu
firstMove :- moveCnt(C), C=3, firstStone(S1,S2), myMove( 3,X1,Y1), myMove( 4,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.

 % navazujici uhlopricka smerem nahoru
firstMove :- moveCnt(C), C=3, firstStone(S1,S2), myMove(32,X1,Y1), myMove(42,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.

 % jinz tah
firstMove :- moveCnt(C), C=3, firstStone(S1,S2), myMove(33,X1,Y1), myMove(43,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.

/** end */




/**
 * pokracuji 2. a 3. pultahem (moveCnt je 2)
 * zacinal jsem
 * overim, ze jsou pole volna a polozim kameny
 */

% prvni uhlopricka
firstMove :- moveCnt(C), C=2, firstStone(S1,S2), myMove( 5,X1,Y1), myMove( 6,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.

% bylo obsazeno, zkousim druhou uhlopricku
firstMove :- moveCnt(C), C=2, firstStone(S1,S2), myMove(52,X1,Y1), myMove(62,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.

% orizontalne / vertikalne
firstMove :- moveCnt(C), C=2, firstStone(S1,S2), myMove(53,X1,Y1), myMove(63,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.
firstMove :- moveCnt(C), C=2, firstStone(S1,S2), myMove(54,X1,Y1), myMove(64,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.

/** end */




% zacinam, prvni tah, nevim jak ziskat svoji barvu, zacinam() ???
firstMove :- FIRST, moveCnt(C), C1 is C+1, retract(moveCnt(C)), assert(moveCnt(C1)), myMove(0,X,Y), playStone(X,Y,white), assert(firstStone(X,Y)), !, fail.

% nezacinam, naleznu souperuv prvni kamen, spocitam posun oproti databazi, overim, ze jsou pole volna a polozim kameny
fisrtMove :- moveCnt(C), C1 is C+2, retract(moveCnt(C)), assert(moveCnt(C1)), findFirstStone(X,Y), firstStone(S1,S2), myMove(1,X1,Y1), myMove(2,X2,Y2), M1 is S1+X1, M2 is S2+Y1, M3 is S1+X2, M4 is S2+Y2, not(stones(M1,M2)), not(stones(M3,M4)), playStones(M1,M2,M3,M4), !, fail.  



% ============ pomocne metody ==============

% nezacinam, naleznu jiz polozeny prvni kamen, jak ziskam plochu jiz polozenych kamenu ???
findFirstStone(X,Y) :- stone(X,Y).


 

