tableSize(19).

:- dynamic stone/4.

inField(X) :- tableSize(S), between(1,S,X).
inField(X,Y) :- inField(X), inField(Y).

color(black).
color(white).

kindOfMove(myFirst).
kindOfMove(mySecond).
kindOfMove(otherFirst).
kindOfMove(otherSecond).

:- dynamic currentKindOfMove/1.
:- dynamic storedKindOfMove/1.

nextKindOfMove(myFirst, mySecond).
nextKindOfMove(mySecond, otherFirst).
nextKindOfMove(otherFirst, otherSecond).
nextKindOfMove(otherSecond, myFirst).

assertNextKindOfMove :-
	currentKindOfMove(K1),
	nextKindOfMove(K1,K2),
	retract(currentKindOfMove(K1)),
	assert(currentKindOfMove(K2)).

assertPrevKindOfMove :-
	currentKindOfMove(K1),
	nextKindOfMove(K2,K1),
	retract(currentKindOfMove(K1)),
	assert(currentKindOfMove(K2)).

storeKindOfMove :-
	currentKindOfMove(K1),
	retractall(storedKindOfMove(_)),
	assert(storedKindOfMove(K1)).

loadKindOfMove :-
	storedKindOfMove(K1),
	retractall(currentKindOfMove(_)),
	assert(currentKindOfMove(K1)).

myColor(black).
otherColor(white).
other(black, white).
other(white, black).

colorToMove(C) :- maxToMove, myColor(C), !.
colorToMove(C) :- minToMove, otherColor(C), !.
colorNotToMove(C) :- minToMove, myColor(C), !.
colorNotToMove(C) :- maxToMove, otherColor(C), !.

minToMove :- currentKindOfMove(otherFirst), !; currentKindOfMove(otherSecond), !.
maxToMove :- currentKindOfMove(myFirst), !; currentKindOfMove(mySecond), !.

firstHalfMove :- currentKindOfMove(myFirst), !; currentKindOfMove(otherFirst), !.
secondHalfMove :- currentKindOfMove(mySecond), !; currentKindOfMove(otherSecond), !.



playStone(X,Y,_) :- stone(X,Y,_,_),
	debugwrite("The position is occupied by another stone\n", []),
	!, fail.

% muj uvodni tah
playStone(X,Y,Color) :- myColor(Color), not(currentKindOfMove(_)),
 	debugwrite('playStone - first move %w,%w,%w\n', [X,Y,Color]),
	assert(stone(X,Y,Color,false)),
	incWindows(X,Y,Color),
	assert(currentKindOfMove(otherFirst)), !.

% jeho uvodni tah
playStone(X,Y,Color) :- otherColor(Color), not(currentKindOfMove(_)),
 	debugwrite('playStone - first move %w,%w,%w\n', [X,Y,Color]),
	assert(stone(X,Y,Color,false)),
	incWindows(X,Y,Color),
	assert(currentKindOfMove(myFirst)), !.

playStone(X,Y,Color) :- 
 	debugwrite('playStone %w,%w,%w\n', [X,Y,Color]),
	assert(stone(X,Y,Color,false)),
	incWindows(X,Y,Color),
	assertNextKindOfMove, !.

% Dobre na testovani v konzoli
playStone(X,Y) :- colorToMove(C),
	playStone(X,Y,C).

playStones(X1,Y1,X2,Y2) :- 
	playStone(X1,Y1),
	playStone(X2,Y2).

retractStone(X,Y) :- stone(X,Y,C,_),
 	debugwrite('retractStone %w,%w\n', [X,Y]),
	retractall(stone(X,Y,_,_)),
	decWindows(X,Y,C),
	assertPrevKindOfMove.

resetGame :-
	retractall(stone(_,_,_,_)),
	retractall(currentKindOfMove(_)),
	resetWindows.

% Debugovaci vystup - pred odevzdanim odkomentovat
%debugwrite(_,_) :- !.
debugwrite(T, Args) :- 	writef("DEBUG: "),	writef(T, Args).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%find row and find column
getColorInPos(Table,X,Y,Color) :- nth0(X,Table,FX), nth0(Y,FX,Color). 

initTable(X, 0, _, X).
initTable(X, CountRow, CountCol, [Row|R]) :-
	Count is CountRow-1,
	initRow([], Count, CountCol, Row),
	initTable(X, Count, CountCol, R).

initRow(X, _, 0, X).
initRow(X, Y, CountCol, [empty|R]) :-
	Count is CountCol-1,
	initRow(X, Y, Count, R).