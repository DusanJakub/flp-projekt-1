:- consult(connect6).

test01 :-
	resetGame,
	initWindowPositions,
	playStone(10,10,black),
	playStone(1,1,white),
	playStone(1,2,white),
	playStone(10,11,black),
	playStone(10,12,black),
	playStone(19,1,white),
	playStone(19,2,white),
	playStone(10,13,black),
	playStone(10,14,black),
	%playStone(19,10,white),
	%playStone(19,11,white),
	timeout(5),
	profile(alphabeta(X)).

test02 :-
	resetGame,
	initWindowPositions,
	playStone(10,10,black),
	playStones(11,11,12,10),
	playStones(9,11,10,12),

	playStones(13,9,14,8),
	playStones(15,7,9,12),

	playStones(13,11,11,9),
	profile(alphabeta(X)).

test03 :-
	resetGame,
	initWindowPositions,
	playStone(10,10,black),
	playStone(1,1,white),
	playStone(1,2,white),
	playStone(10,11,black),
	playStone(10,12,black),
	playStone(19,1,white),
	playStone(19,2,white),
	playStone(10,13,black),
	playStone(10,14,black),
	%playStone(19,10,white),
	%playStone(19,11,white),
	timeout(3),
	alphabetaStart(X,2).
