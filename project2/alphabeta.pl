/********************************************************
 ********************* POZOR: ***************************

 Tohle je naprosto netestovana verze! Jde tu o definici 
 rozhrani, ktera je snad definitivni, ale fungovat 
 to jeste nebude !!!

 ********************************************************
 ********************************************************/

:- consult(evaluate).
:- consult(moves).
:- consult(rules).
:- consult(window).

% Operatory pro porovnavani s nekonecnem
:- op(700,xfx,>>>).
:- op(700,xfx,<<<).
:- op(700,xfx,>>>=).
:- op(700,xfx,=<<<).
infinity >>> -infinity :- !.
infinity >>> B :- number(B), !.
A >>> -infinity :- number(A), !.
A >>> B :- number(A), number(B), A > B, !.

-infinity <<< infinity :- !.
-infinity <<< B :- number(B), !.
A <<< infinity :- number(A), !.
A <<< B :- number(A), number(B), A < B, !.

A >>>= B :- A = B, !; A >>> B.
A =<<< B :- A = B, !; A <<< B.

% TODO: tohle je vstupni bod do alfabety, tady se bude pocitat cas atd.
% TODO: prozatim je tu jen natvrdo nastavena hloubka
alphabetaStart(GoodMoves, Depth) :-
  endTime(END),
  debugwrite('alfabeta max end time %w\n',[END]),
  get_time(T1),
  alphabeta(Depth, GoodMoves),
  get_time(T2),
  T3 is T2 - T1,
  debugwrite('alfabeta run time %w\n',[T3]),
  ( (T3*2 + T2) < END -> NextDepth is Depth + 1, alphabetaStart(GoodMoves, NextDepth) ; true ).

alphabeta(GoodMoves) :-
  alphabeta(2, GoodMoves).

alphabeta( Depth, GoodMoves) :-
  debugwrite('alfabeta d=%w\n',[Depth]),
  alphabeta(-infinity, infinity, Depth, GoodMoves, _),
  debugwrite('alfabeta result=', []),
  debugwrite('%w\n',[GoodMoves]).

%!      alphabeta(+Pos:list, +Alpha:int, +Beta:int, -GoodPos:list, -Val:int) is det.
%
%   Calculates the best move using alpha beta pruning.
%     Pos           Current state of the game (list of played moves)
%     Aplha, Beta   The bounds
%     GoodPos       The state of the game after the best move has been played
%     Val           Suitability of the new game state

alphabeta( Alpha, Beta, 0, [], Val) :-
  %debugwrite('limit: a=%w b=%w d=%w', [Alpha, Beta, 0]),
  evaluate( Val), 
  %debugwrite(' val=%w\n', [Val]),
  !.                  % We don't have time to drill any deeper

alphabeta( Alpha, Beta, Depth, GoodMoves, Val) :-
  %debugwrite('a=%w b=%w d=%w\n', [Alpha, Beta, Depth]),
  moves( MoveList), !,   
  %debugwrite('%w\n',[MoveList]),              % Legal moves in Pos
  Depth2 is Depth-1,
  boundedbest( MoveList, Alpha, Beta, Depth2, GoodMoves, Val),
  %debugwrite(' goodmoves=%w\n', [GoodMoves]).
  !.

alphabeta( Alpha, Beta, Depth, [], Val) :-
  %debugwrite('terminal: a=%w b=%w d=%w\n', [Alpha, Beta, Depth]),
  evaluate( Val), 
  %debugwrite(' val=%w\n', [Val]).                    % Terminal Pos has no successors
  !.


%!  boundedbest(+PosList:list, +Alpha:int, +Beta:int, -GoodPos:list, -GoodVal:int)
%   Finds the best move from a list of possible moves
%     PosList       Current state of the game (list of played moves)
%     Aplha, Beta   The bounds
%     GoodPos       The state of the game after the best move has been played
%     GoodVal       Suitability of the new game state
boundedbest( [(X,Y) | MovesTail], Alpha, Beta, Depth, GoodMoves, GoodVal) :-
  colorToMove(C),  playStone(X,Y,C),
  alphabeta( Alpha, Beta, Depth, GoodMovesTail, Val),
  retractStone(X,Y),
  goodenough( MovesTail, Alpha, Beta, Depth, [(X,Y) | GoodMovesTail], Val, GoodMoves, GoodVal).

% Tohle nesmi nikdy nastat
boundedbest( [], _, _, _, _, _) :- debugwrite("boundedbest[]", []), fail.

goodenough( [], _, _, _, Moves, Val, Moves, Val) :- !.
% This predicate stops the other moves from being generated. 
% The predicate will succeed but the result will be eliminated higher in the tree by betterof
goodenough( _, Alpha, Beta, _, Moves, Val, Moves, Val) :-        
  maxToMove, Val >>>= Beta, !       % MAX prefers the greater value
  ;
  minToMove, Val =<<< Alpha, !.     % MIN prefers the lesser value 

% Now the value has passed the alpha-beta check so we will compute further.
% The bounds are update from the result, the resulting moves are evaluated and the best one is selected.
goodenough( MoveList, Alpha, Beta, Depth, Moves, Val, GoodMoves, GoodVal)  :-
  newbounds( Alpha, Beta, Moves, Val, NewAlpha, NewBeta),    % Refine bounds  
  boundedbest( MoveList, NewAlpha, NewBeta, Depth, Moves1, Val1),
  betterof( Moves, Val, Moves1, Val1, GoodMoves, GoodVal).



newbounds(Alpha, Beta, _, Val, Val, Beta) :-
  maxToMove, Val >>>= Alpha, !.

newbounds(Alpha, Beta, _, Val, Alpha, Val) :-
  minToMove, Val =<<< Beta, !.

newbounds(Alpha, Beta, _, _, Alpha, Beta).


betterof( M1, E1, M2, E2, M, E) :- maxToMove, !,
  (E1 >>>= E2 -> E = E1, M = M1; E = E2, M = M2)
  , debugwrite("max(%w %w) = %w\n", [E1, E2, E])
  .


betterof( M1, E1, M2, E2, M, E) :- minToMove, !,
  (E1 =<<< E2 -> E = E1, M = M1; E = E2, M = M2)
  , debugwrite("min(%w %w) = %w\n", [E1, E2, E])
  .

/*
betterof( Moves, Val, _, Val1, Moves, Val) :-
  minToMove, Val =<<< Val1, debugwrite("min: %w =<<< %w\n", [Val, Val1]), !
  ;
  maxToMove, Val >>>= Val1, debugwrite("max: %w >>>= %w\n", [Val, Val1]), !.

betterof( _, Val, Moves1, Val1, Moves1, Val1) :- debugwrite("not better: %w >>>=<<< %w\n", [Val, Val1]), !.


  betterof( Moves, Val, Moves1, Val1, GoodMoves, GoodVal) :- debugwrite("Fail betterof%w", [[Moves, Val, Moves1, Val1, GoodMoves, GoodVal]]), fail.

*/



