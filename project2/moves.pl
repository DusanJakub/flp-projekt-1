:- consult(rules).
:- consult(window).

% Pokud je akt. stav vitezstvim jenoho hrace, hra konci, takze neni mozny zadny dalsi tah
moves(_) :- instantWin(_), !, fail.

% TODO: Chytry vyber tahu je zakomentovany, nejdriv se musi opravit alfabeta

% 1. pokud je mozne vyhrat na 2 pultahy a jsem v prvni puli tahu, resp. na 1 tah a jsem v druhe puli, 
% nic dalsiho neresim
moves(MoveList) :- firstHalfMove, colorToMove(C1), threat(C1,_),
	setof((X,Y), W^(threat(C1,W), emptyIn(X,Y,W)), MoveList), !.
moves(MoveList) :- secondHalfMove, colorToMove(C1), threat1(C1,_),
	setof((X,Y), W^(threat1(C1,W), emptyIn(X,Y,W)), MoveList), !.

% 2. ve hre jsou souperovy hrozby - musim je vykryt, nic dalsiho neresim
moves(MoveList) :- colorNotToMove(C2), threat(C2,_),
	setof((X,Y), W^(threat(C2,W), emptyIn(X,Y,W)), MoveList), !.

% 3. vratim vsechna pole, ktera jsou ve stejnem radku, sloupci ci diagonale ve vzdalenosti nejvyse 5 od existujiciho kamene
moves(MoveList) :-
	setof((X,Y), D^C^X0^Y0^(inField(X,Y), not(stone(X,Y,_,_)), stone(X0,Y0,C,_), between(1,2,D), influence(X0,Y0,D,X,Y)), MoveList).




influence(X,Y,D,X,Y2)  :- Y2 is Y+D.
influence(X,Y,D,X,Y2)  :- Y2 is Y-D.
influence(X,Y,D,X2,Y)  :- X2 is X+D.
influence(X,Y,D,X2,Y)  :- X2 is X-D.
influence(X,Y,D,X2,Y2) :- X2 is X+D, Y2 is Y+D.
influence(X,Y,D,X2,Y2) :- X2 is X-D, Y2 is Y-D.
influence(X,Y,D,X2,Y2) :- X2 is X+D, Y2 is Y-D.
influence(X,Y,D,X2,Y2) :- X2 is X-D, Y2 is Y+D.

