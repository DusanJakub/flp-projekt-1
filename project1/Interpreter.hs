module Interpreter (interpretPascal) where
{-- 
    Projekt:  FLP - Funkcionální projekt    
    Varianta: 03 - Interpret imperativního jazyka FLP-2014-Pascal  
    Team:     FLP 33
              Dusan Jakub     <xjakub19[at]stud.fit.vutbr.cz>
              Jakub Varadinek <xvarad01[at]stud.fit.vutbr.cz>
              Martin Brazdil  <xbrazd14[at]stud.fit.vutbr.cz>
              Marek Zak       <xzakma01[at]stud.fit.vutbr.cz>

    Popis:    Modul pro interpret jazyka PASCAL upraveneho pro predmet FLP.
    Autori:   Dusan Jakub     <xjakub19[at]stud.fit.vutbr.cz>
              Jakub Varadinek <xvarad01[at]stud.fit.vutbr.cz>
    Datum:    12.3.2014
--}
import Types
import Parser  -- for ReadStmt
import qualified Data.Map as Map 
import qualified Data.Maybe as Maybe

-- vstupni bod interpretu. Provadi interpretaci dat z parseru.
interpretPascal :: Program -> IO [[Store]]
interpretPascal program =  
	case Types.lookupFunc "" program of 
		(Just f) -> intrp program [[fromScope (globals program)],[]] (code f)

{-- dvojice (hodnota, promenna) --}
fromScope :: Scope -> Store
fromScope scope = Map.fromList $ map (\var -> (var,defaultValue $ varType var)) $ variables scope
	where 
		defaultValue IntType = IntValue 0
		defaultValue DoubleType = DoubleValue 0.0
		defaultValue StringType = StringValue ""

--funkce vlozi promenou a jeji hodnotu do seznamu promennych (store)
put :: Variable -> Value -> [[Store]] -> [[Store]]
put var val [] = error $ "Variable not in store " ++ (show var)
put var val ((top:rest):others) | varType var == valType val = 
	if Map.member var top
		then ((Map.insert var val top : rest ) : others)
		else ((top : rest) : put var val others)

--funkce ziskam hodnotu promenne ze seznamu promennych (store)
get :: Variable -> [[Store]] -> Value
get var [] = error $ "Variable not in store " ++ (show var) 
get var ((store:_):others) = 
	case Map.lookup var store of
		(Just val) -> val
		(Nothing)  -> get var others

{--
   Nasledujici metody slouzi jako pomocne pri volani funkci.
--}

--vlozi do stores novy lokalni store pro danou funkci
addLocalStore :: Store -> [[Store]] -> [[Store]]
addLocalStore local stores = [(stores !! 0),local:(stores !! 1)]

--vraci vyhodnocene argumenty funkce
getEvaluatedArguments :: Program -> [[Store]] -> [Expr] -> [IO (Value, [[Store]])]
getEvaluatedArguments program stores paramEprs = do map (\x -> evalExpr program stores x) paramEprs

--ziska promenou ze store podle jmena promenne
getVariableByName :: String -> Store -> Variable
getVariableByName name localVars = (Map.keys (Map.filterWithKey (\k _ -> (varName k) == name) localVars)) !! 0

--ziska promenou, ktera slouzi pro navratovou hodnotu funkce
getFuncReturnVar :: Function -> Store -> Variable
getFuncReturnVar f localVars = getVariableByName (name f) localVars

--ziska navratovou hodnotu funkce
getFuncReturnValue :: Function -> Store -> [[Store]] -> Value
getFuncReturnValue f localVars stores = get (getFuncReturnVar f localVars) stores

--ziska hodnotu z vyhodnoceneho evalExpr
getValueFromEvalutedExpr :: IO (Value, [[Store]]) -> IO Value
getValueFromEvalutedExpr exprEv = do 
	(val, ns) <- exprEv
	return val

--prevede seznam vysledku vyhodnoceni a stores na seznam pouze s vysledkem
getValuesFromEvalutedExprs :: [IO (Value, [[Store]])] -> [IO Value]
getValuesFromEvalutedExprs exprsEv = map (\x -> getValueFromEvalutedExpr x) exprsEv

--prevede seznam ve kterem jsou argumenty funkce na seznam, kde jsou jiz vytvorene promenne funkce ze store
getArgumentsFromStore :: [Variable] -> Store -> [Variable]
getArgumentsFromStore args localVars = map (\x -> getVariableByName (varName x) localVars) args

--funkce vlozi promenou a hodnotu do zvoleneho store
putToSingleStore :: Variable -> Value -> Store -> Store
putToSingleStore var val store = (Map.insert var val store)

--funkce nastavi hodnotu argumentu
addValueToArgument :: Variable -> IO Value -> Store -> IO Store
addValueToArgument arg val store = do
	value <- val
	return $ putToSingleStore arg value store

--funkce umisti vsechny vypocitane hodnoty do argumentu funkce
addAllValuesToArguments :: [Variable] -> [IO Value] -> Store -> IO Store
addAllValuesToArguments [] [] store = return store
addAllValuesToArguments (arg : arguments) (val : values) store = do
	updatedStore <- addValueToArgument arg val store
	updatedStore2 <- addAllValuesToArguments arguments values updatedStore
	return updatedStore2 

--actualizeGlobalStore :: [[Store]] -> [[Store]] -> [[Store]]
--actualizeGlobalStore newStores oldStores = [newStores !! 0, oldStores !! 1]

--evalExpr vyhodnuje vsechny vyrazy. Volani funkce je take povazovano za vyraz.
evalExpr :: Program -> [[Store]] -> Expr -> IO (Value, [[Store]])

--konstanty
evalExpr program stores (IntConst i)    = return (IntValue i, stores)
evalExpr program stores (DoubleConst d) = return (DoubleValue d, stores)
evalExpr program stores (StringConst s) = return (StringValue s, stores)

--ziskani dat z promenne
evalExpr program stores (VarExpr var)   = return (get var stores, stores)

--volani funkce
evalExpr program stores (Call fun params) = do
	let f = case Types.lookupFunc (funcSignature fun) program of 
				 (Just res) -> res
	let localVars = fromScope (vars f)
	let args = getArgumentsFromStore (arguments f) localVars
	let evaluteValues = getValuesFromEvalutedExprs (getEvaluatedArguments program stores params)
	storeWithParams <- addAllValuesToArguments args evaluteValues localVars
	let newStores = addLocalStore storeWithParams stores

	storeAfterCall <- intrp program newStores (code f)
	let ret = getFuncReturnValue f localVars storeAfterCall
	return (ret, [storeAfterCall !! 0, stores !! 1])

--specialni prevodni funkce z int do double
evalExpr program stores (IntToDouble e) = do
	r <- evalExpr program stores e
	return $ case (r) of
		((IntValue i), newStores) -> (DoubleValue (fromIntegral i), newStores)

--operatory porovnani
evalExpr program stores (Compare c a b) = do
	ra <- evalExpr program stores a 
	rb <- evalExpr program (snd ra) b
	let newStores = snd rb
	return $ case (fst ra, fst rb) of
		(IntValue    va, IntValue    vb) -> (BoolValue (comp c va vb), newStores)
		(DoubleValue va, DoubleValue vb) -> (BoolValue (comp c va vb), newStores)
		(StringValue va, StringValue vb) -> (BoolValue (comp c va vb), newStores)
		(BoolValue   va, BoolValue   vb) -> (BoolValue (comp c va vb), newStores)
	where 
		comp Types.LT = (<)
		comp Types.LE = (<=)
		comp Types.EQ = (==)
		comp Types.NE = (/=)
		comp Types.GE = (>=)
		comp Types.GT = (>)

--operator nasobeni
evalExpr program stores (Mult a b) = do
	ra <- evalExpr program stores a 
	rb <- evalExpr program (snd ra) b
	let newStores = snd rb
	return $ case (fst ra, fst rb) of
		(IntValue 	 va, IntValue    vb) -> (IntValue    (va * vb), newStores)
		(DoubleValue va, DoubleValue vb) -> (DoubleValue (va * vb), newStores)

--operator scitani
evalExpr program stores (Add a b) = do
	ra <- evalExpr program stores a 
	rb <- evalExpr program (snd ra) b
	let newStores = snd rb
	return $ case (fst ra, fst rb) of
		(IntValue 	 va, IntValue    vb) -> (IntValue    (va + vb),  newStores)
		(DoubleValue va, DoubleValue vb) -> (DoubleValue (va + vb),  newStores)
		(StringValue va, StringValue vb) -> (StringValue (va ++ vb), newStores)

--operator odcitani
evalExpr program stores (Sub a b) = do
	ra <- evalExpr program stores a 
	rb <- evalExpr program (snd ra) b
	let newStores = snd rb
	return $ case (fst ra, fst rb) of
		(IntValue 	 va, IntValue    vb) -> (IntValue    (va - vb), newStores)
		(DoubleValue va, DoubleValue vb) -> (DoubleValue (va - vb), newStores)

--operator deleni
evalExpr program stores (Div a b) = do
	ra <- evalExpr program stores a 
	rb <- evalExpr program (snd ra) b
	let newStores = snd rb
	return $ case (fst ra, fst rb) of
		(IntValue 	 va, IntValue    vb) -> (IntValue (va `div` vb), newStores)

--nasledujici funkce provadi interpretaci jednotlivych prikazu
intrp :: Program -> [[Store]] -> Stmt -> IO [[Store]]
--obluha standardnich prikazu
intrp program stores (SeqStmt []) = return stores
intrp program stores (SeqStmt (x:xs)) = do
	newStores <- intrp program stores x 
	intrp program newStores (SeqStmt xs)

--oblsuha prikazu prirazeni
intrp program stores (AssignStmt var expr) = do
	ret <- evalExpr program stores expr
	return $ put var (fst ret) (snd ret)

--obsluha pro prikaz while
intrp program stores whileStmt@(WhileStmt expr stmt) = do
	ret <- evalExpr program stores expr
	case ret of 
		((BoolValue True), newStores)  -> do { s <- intrp program newStores stmt; intrp program s whileStmt }
		((BoolValue False), newStores) -> return newStores

--obluha pro prikaz if
intrp program stores (CondStmt expr pos neg) = do
	ret <- evalExpr program stores expr
	case ret of 
		((BoolValue True), newStores) -> intrp program newStores pos
		((BoolValue False), newStores) -> intrp program newStores neg

--obsluha specialniho prikazu writeln
intrp program stores (WriteStmt expr) = do
	(val, newStores) <- evalExpr program stores expr
	case val of 
		(StringValue v) -> putStrLn v
		(IntValue v)    -> print v
		(DoubleValue v) -> print v
		(BoolValue v)   -> print v
	return newStores

--obsluha specialniho prikazu readln
intrp program stores (ReadStmt var) = do
	value <- case varType var of 
		StringType 	-> getLine >>= (\line -> return $ StringValue line)
		IntType		-> readInt
		DoubleType	-> readDouble
	return $ put var value stores

--funkce pro obsluhu read pri nacitani cisla typu int
readInt :: IO Value
readInt = do 
	line <- getLine
	case parseInteger line of 
		(Right i) -> return $ IntValue i
		(Left _) -> error "Invalid integer value"

--funkce pro obsluhu read pri nacitani cisla typu double
readDouble :: IO Value
readDouble = do 
	line <- getLine
	case parseDouble line of 
		(Right i) -> return $ DoubleValue i
		(Left e) -> error "Invalid double value"
