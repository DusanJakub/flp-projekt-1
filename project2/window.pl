
% spocita kameny dane barvy v danem okne
stonesInRow(Count,Color,W) :- window(W,Color,Count), other(Color, Color2), window(W,Color2,0).

% hrac timto tahem zvitezil
instantWin(C) :- stonesInRow(6,C,_).
% vitezstvi barvy v 1 tahu
threat1(C,W) :- stonesInRow(5,C,W).
% vitezstvi barvy ve 2 tazich
threat2(C,W) :- stonesInRow(4,C,W).

% jakakoli hrozba
threat(Color,W) :- threat1(Color,W); threat2(Color,W).

% zjisti, zda se v seznamu W nachazi prazdne pole (barva)
% W je [(X,Y,h|d|d1|d2),...]
emptyIn(X,Y,W) :- inWindow(W,X,Y), not(stone(X,Y,_,_)).

% overeni souradnice, zda je k okne
inWindow((Xstart, Ystart, h),  X, Ystart) :- Xmax is Xstart + 5, between(Xstart, Xmax, X).
inWindow((Xstart, Ystart, v),  Xstart, Y) :- Ymax is Ystart + 5, between(Ystart, Ymax, Y).
inWindow((Xstart, Ystart, d1), X, Y)      :- Xmax is Xstart + 5, between(Xstart, Xmax, X), Y is Ystart + X - Xstart.
inWindow((Xstart, Ystart, d2), X, Y)      :- Xmax is Xstart + 5, between(Xstart, Xmax, X), Y is Ystart + Xstart - X.

% (X,Y,direction),color,color_count
:- dynamic window/1.

% vytvori databazi okenek (4 smery)
initWindowPositions :-
	forall(validStart(X,Y,D),   % vygenerovani smeru oken
	( 
		assert(window((X,Y,D),white,0)), 
		assert(window((X,Y,D),black,0)) 
	)).

% vygenerovani validnich souradnic pro okna
validStart(X,Y,h)  :- tableSize(S), Max is S-5, between(1,Max,X), between(1,S,  Y).
validStart(X,Y,v)  :- tableSize(S), Max is S-5, between(1,S,  X), between(1,Max,Y).
validStart(X,Y,d1) :- tableSize(S), Max is S-5, between(1,Max,X), between(1,Max,Y).
validStart(X,Y,d2) :- tableSize(S), Max is S-5, between(1,Max,X), between(6,S,  Y).

% navysi, resp. snizi pocet kamenu dane barvy o 1
incWindows(X,Y,C) :- updateWindows(X,Y,C,1).
decWindows(X,Y,C) :- updateWindows(X,Y,C,-1).

% upravi pocet kamenu tak, ze pricte k aktualnimu poctu zadanou hodnotu "Val"
updateWindows(X,Y,Color,Val) :- 
	forall((window(W,Color,Count), inWindow(W,X,Y)), % nacteni a overeni pozice
	(
		retract(window(W,Color,Count)), % vyjmuti z DB
		Cnt is Count+Val,
		assert(window(W,Color,Cnt))     % vlozeni zpet s novou hodnotou
	)).

% vymaze DB s okny
resetWindows :-
	retractall(window(_,_,_)). 
