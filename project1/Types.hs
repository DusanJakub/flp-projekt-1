module Types where
{-- 
    Projekt:  FLP - Funkcionální projekt    
    Varianta: 03 - Interpret imperativního jazyka FLP-2014-Pascal  
    Team:     FLP 33
              Dusan Jakub     <xjakub19[at]stud.fit.vutbr.cz>
              Jakub Varadinek <xvarad01[at]stud.fit.vutbr.cz>
              Martin Brazdil  <xbrazd14[at]stud.fit.vutbr.cz>
              Marek Zak       <xzakma01[at]stud.fit.vutbr.cz>

    Popis:    Modul definujici uzivatelske datove typy a funkce pro praci s nimi.
    Autori:   Dusan Jakub    <xjakub19[at]stud.fit.vutbr.cz>
              Martin Brazdil <xbrazd14[at]stud.fit.vutbr.cz>
    Datum:    12.3.2014
--}
import qualified Data.Map as Map 
import Control.Applicative

{-- Typ pro mapovani nazvu funkce na info o funkci. --}
type FunctionMap = Map.Map String Function

{-- Struktura obsahujici data celeho programu (funkce a globalni promenne). 
    Klic u "tabulky" funkcí obsahuje signaturu funkce. 
    Ta má tvar: <name>(<argsType>) viz funkce funcSignature --}
data Program = Program {
  functions :: FunctionMap,
  globals :: Scope
} deriving Show

{-- Struktura obahující informace o funkci (nazev, navratovy typ, seznam
    argumentu, lokalni promenne, telo, priznak deklarace/definice).
    Pozn.: defined ... False = deklarovana, True = definovana 
           vars ... obsahuje i globalni promenne (viz Scope) --}
data Function = Function {
  name :: String, 
  returnType :: DataType,
  arguments :: [Variable],
  vars :: Scope,
  code :: Stmt,
  defined :: Bool  
} deriving (Show)

-- ziskani signatury funkce z promenne typu Function
funcSignature :: Function -> String
funcSignature f@(Function n _ args _ _ _) = 
  funcSignatureStr n (varsSignature args)

-- vytvoreni signatury z retezcu (spojeni do spravneho tvaru)  
funcSignatureStr :: String -> String -> String
funcSignatureStr id types = id ++ "(" ++ types ++ ")"

-- ziskani typove signatury seznamu vyrazu
exprsSignature :: [Expr] -> String
exprsSignature exprs = (exprs >>= (\e -> exprSignature e))

-- ziskani typve signatury daneho vyrazu
exprSignature :: Expr -> String
exprSignature e = typeSignature (exprType e)

-- ziskani typove signatury seznamu promennych
varsSignature :: [Variable] -> String
varsSignature vars = (vars >>= (\v -> varSignature v))

-- ziskani typove signatury promenne
varSignature :: Variable -> String
varSignature v@(Var _ _ t) = typeSignature t

-- ziskani signatury daneho datoveho typu
typeSignature :: DataType -> String
typeSignature t = 
  case t of
    (IntType)     -> "I"
    (DoubleType)  -> "D"
    (StringType)  -> "S"
    (BooleanType) -> "B"
    otherwise     -> ""

{-- Struktura uchovavajici informace o promenne (uroven zanoreni ve scope,
    nazev, typ). --}
data Variable = Var Int String DataType deriving (Show, Eq, Ord)

-- ziska typ zadane promenne
varType :: Variable -> DataType
varType (Var _ _ t) = t

-- ziska nazev (identifikator) zadane promenne
varName :: Variable -> String
varName (Var _ n _) = n

{-- Store je seznam seznamu, kde prvni uroven obsahuje promenne (loklani a globalni) 
    a druha uroven nahrazuje zasobnik pri volani funkci.
    Prikad seznamu behem interpretovani kodu:
  [ [ global store - always just one  ], [ store of the last fnc called - top of the stack,
                       ...
                       store of the first fnc called - bottom of the stack ]
--}
type Store = Map.Map Variable Value

{-- Typ pro mapovani nazvu promenne na datovy typ. --}
type ScopeMap = Map.Map String DataType
{-- Struktura pro uchovani promennych v ruznych urovnich programu
    (globalni vs. lokalni promenne). --}
data Scope  = RootScope ScopeMap
            | LocalScope Scope ScopeMap 
            | EmptyScope
            deriving Show

-- ziskani lokalni scope mapy   
scopeMapLocal :: Scope -> ScopeMap
scopeMapLocal (LocalScope parent scMap) = scMap

-- ziskani scope rodice
scopeParent :: Scope -> Scope      
scopeParent (LocalScope parent _) = parent
      
-- ziskani scope mapy v dane urovni scope    
scopeMap :: Scope -> ScopeMap      
scopeMap (RootScope scMap) = scMap 
scopeMap (LocalScope parent scMap) = scMap

-- ziskani urovne daneho scope 
scopeLevel :: Scope -> Int
scopeLevel (RootScope _) = 0
scopeLevel (LocalScope parent _) = 1 + scopeLevel parent

-- nalezeni funkce v zadane mape 
lookupFuncMap :: String -> FunctionMap -> Maybe Function
lookupFuncMap name map = Map.lookup name map >>= \f -> Just (recreateFunction f)

-- znovu vytvoreni promenne funkce
recreateFunction :: Function -> Function
recreateFunction f = Function (name f) (returnType f) (arguments f) (vars f) (code f) (defined f)

-- nalezeni funkce v mape v promenne Program 
lookupFunc :: String -> Program -> Maybe Function
lookupFunc name program@(Program map glob) = lookupFuncMap name map

-- overeni dekralarace promenne s nazvem, scopem, urovni 
lookupVarMap :: String -> ScopeMap -> Int -> Maybe Variable
lookupVarMap name map level = Map.lookup name map >>= \t -> Just (Var level name t)

-- overeni dekralarace promenne s nazvem name 
lookupVar :: String -> Scope -> Maybe Variable
lookupVar name scope@(RootScope map) = lookupVarMap name map (scopeLevel scope)
lookupVar name scope@(LocalScope parent map) = 
  lookupVarMap name map (scopeLevel scope) <|> Types.lookupVar name parent

-- prevadi scope na seznam variables (promennych)
variables :: Scope -> [Variable]
variables scope = map (\(k,v) -> Var level k v) $ Map.toList m
  where m = case scope of (RootScope map)    -> map
                          (LocalScope _ map) -> map
        level = scopeLevel scope

{-- Struktury pro uchovani vyrazu (nasobeni, podil, scitani, odcitani,
    porovnani, volani fce, prevod int na double, konstanty, promenna). --}
data Expr = Mult Expr Expr
          | Div Expr Expr
          | Add  Expr Expr
          | Sub Expr Expr
          | Compare ValueComparator Expr Expr
          | Call Function [Expr]
          | IntToDouble Expr
          | IntConst Integer | DoubleConst Double | StringConst String | VarExpr Variable
           deriving Show
           
{-- Struktury pro uchovani hodnot dle typu (int, double, string, boolen). --}
data Value =  IntValue Integer
            | DoubleValue Double
            | StringValue String
            | BoolValue Bool
            deriving (Show, Eq)
            
-- funkce pro ziskani dat. typu dane hodnoty
valType :: Value -> DataType
valType (IntValue _)    = IntType
valType (DoubleValue _) = DoubleType
valType (StringValue _) = StringType
valType (BoolValue _)   = BooleanType

-- porovnavani hodnot
instance Ord Value where
  IntValue    a < IntValue    b = a < b
  DoubleValue a < DoubleValue b = a < b
  StringValue a < StringValue b = a < b
  BoolValue   a < BoolValue   b = a < b

{-- Vycet typu (moznosti) porovnani. --}
data ValueComparator = LT | LE | EQ | NE | GE | GT deriving (Show)

-- ziskani dat. typu vyrazu
exprType :: Expr -> DataType
exprType (IntConst _)               = IntType
exprType (DoubleConst _)            = DoubleType
exprType (StringConst _)            = StringType
exprType (VarExpr (Var _ _ t))      = t
exprType (IntToDouble _)            = DoubleType
exprType (Compare _ _ _)            = BooleanType
exprType (Call f _)                 = returnType f
exprType (Mult a b) | exprType a == exprType b = exprType a 
exprType (Div  a b) | exprType a == exprType b = exprType a 
exprType (Add  a b) | exprType a == exprType b = exprType a 
exprType (Sub a  b) | exprType a == exprType b = exprType a 

{-- Struktura pro uchovani prikazu. --}
data Stmt = SeqStmt [Stmt]
          | AssignStmt Variable Expr
          | WhileStmt Expr Stmt
          | CondStmt Expr Stmt Stmt
          | ReadStmt Variable 
          | WriteStmt Expr
          | EmptyStmt
           deriving (Show)

-- rovnost vyrazu           
-- !!! pouzit pouze pro porovnani na EmptySmtm !!!
instance Eq Stmt where 
  EmptyStmt == EmptyStmt = True
  _ == _ = False

{-- Vycet datovych typu. --}           
data DataType = IntType | DoubleType | StringType | BooleanType | EmptyType deriving (Show, Eq, Ord)
