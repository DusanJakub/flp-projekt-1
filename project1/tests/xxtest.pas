{ PROJEKT - FLP 2014 }
{ VZOROVY TESTOVACI SOUBOR }

{ globalni identifikatory - vetsina povolenych kombinaci }
var _ : integer, _var : integer, _Var : integer, _VAR : integer, o : integer, _vAR : integer, w : double, x : double, Y : double, z : double, res : double, s1 : string, Ss1 : string;

{ deklarace a definice funkci vcetne pretezovani funkce f1 }

{ funkce vraci 1 }
function f1 () : integer;

function f1 () : integer;
  begin
    f1 := 1
  end

{ funkce vraci dvojnasobek zadaneho cisla }
function f1 (s : double) : double;

function f1 (s : double) : double;
  var res : double; 
  begin
    o := 15;
    res := s * 2;
    f1 := res
  end
  
{ funkce vraci soucin zadanych cisel }
function f1 (s : double, t : double) : double;

function f1 (s : double, t : double) : double;
  var res : double; 
  begin
    res := s * t;
    f1 := res
  end  
  
{ funkce vraci faktorial zadaneho cisla }  
function f3 (x : integer) : integer;

function f3 (x : integer) : integer;
  var tmp : integer, dec : integer, res : integer;
  begin
    if (x < 2) then
      res := 1
    else
    begin
      dec := x - 1;
      { rekurzivni volani funkce f3 }
      tmp := f3(dec);
      res := x * tmp
    end;
    f3 := res
  end  

{ hlavni telo programu }
begin                                                       
  writeln('=====================================================');
  writeln('============  Vzorovy testovaci soubor  =============');
  writeln('=====================================================');

  { prirazeni do promenne typu INT }
  _ := 1;
  { prirazeni do promenne typu DOUBLE }
  w := 1;
  { prirazeni do promenne typu DOUBLE }
  x := 2.0;
  { prirazeni do promenne typu DOUBLE }
  Y := 2e5;
  { prirazeni do promenne typu DOUBLE }
  z := 5E-2;
  { prirazeni do promenne typu DOUBLE }
  res := 5.5;
  
  { prirazeni do promenne typu STRING }
  s1 := '''a';
  { prirazeni do promenne typu STRING }
  Ss1 := 'bcdef';
  
  { prekryv promennych }
  writeln('Prekryv promennych: (prepis o=1 na o=15)');
  
  { globalni promenna o }
  o := 1;
  writeln(o);
  
  { funkce f1 NEMA promennou o => pouzije se globalni promenna }
  writeln('Volam funkci f1(1.0)');
  z := f1(1.0);
  
  { po zavolani funkce je povodni hodnota zmenena }
  writeln(o);
  
  { nedoslo k prepsani uvnitr funkce f1 }
  writeln(res);
  writeln('');
  
  
  { volani funkce f1 }
  writeln('Volani funkce f1:');
  writeln(f1());
  writeln('');
  
  { volani funkce f1 - celociselny argument }
  writeln('Volani funkce f1 s celociselnym argumentem:');
  writeln(f1(1));
  writeln('');
  
  { volani funkce f1 - desetinny argument}
  writeln('Volani funkce f1 s desetinnym argumentem:');
  writeln(f1(2e-1));
  writeln('');
  
  { volani funkce f1 - dva desetinne argumenty}
  writeln('Volani funkce f1 s dvema desetinnymi argumenty:');
  writeln(f1(2e-1, 5e-1));
  writeln('');
  
  { nacteni retezce ze STDIN }
  writeln('Zadejte retezec:');
  readln(s1);
  writeln('Nactena hodnota:');
  writeln(s1);
  writeln('');
  
  { nacteni celociselne hodnoty ze STDIN }
  writeln('Zadejte cislo typu INT:');          
  readln(_);
  writeln('Nactena hodnota:');
  writeln(_);
  writeln('');  

  { nacteni desetinne hodnoty ze STDIN }
  writeln('Zadejte cislo typu DOUBLE:');
  readln(z);
  writeln('Nactena hodnota:');         
  writeln(z);
  writeln('');
  
  { porovnani dvou hodnot a podmineny prikaz }
  { hodnoty jsou typu DOUBLE a INT }
  writeln('Porovnani zadanych ciselnych hodnot:');
  if _ > z then
    writeln('Hodnota typu INT je vetsi')
  else
    writeln('Hodnota typu DOUBLE je vetsi');
    
  writeln('');
  
  { prikaz cyklu WHILE - vypise 5x 'Hello World !!!' }
  writeln('Cyklus WHILE vypise 5x ''Hello World !!!'' :');
  _ := 0;
  
  while _<5 do
    begin
      writeln('Hello World !!!');
      _ := _ + 1
    end;         
  
  writeln('');
  
  { priorita operatoru }
  writeln('x = 1 + 2 * 3');
  _ := 1 + 2 * 3;
  writeln(_);
  writeln('');
  
  writeln('x = 1 * 2 + 3');
  _ := 1 * 2 + 3;
  writeln(_);
  writeln('');
  
  writeln('x = 8 div 4 - 2');
  _ := 8 div 4 - 2;
  writeln(_);
  writeln('');
  
  writeln('x = 8 - 4 div 2');
  _ := 8 - 4 div 2;
  writeln(_);
  writeln('');
  
  { uziti operatoru }
  writeln('Soucet promennych typu STRING:');
  s1 := s1 + Ss1;
  writeln(s1);
  writeln('');
  
  { rekurze - obsazena ve funkci f3 (vypocet faktorialu) }
  writeln('Zadejte cele cislo pro vypocet faktorialu:');
  readln(_);
  writeln('Vypocet faktorialu rekurzi:');
  writeln(f3(_));
  
  { vypocet faktorialu iterativne }
  writeln('Vypocet faktorialu iterativne:');
  
  if (_ < 0) then
    writeln('Faktorial nelze spocitat')
  else
  begin
    _var := 1;
    while (_ > 0) do
    begin
      _var := _var * _;
      _ := _ - 1
    end;
    writeln(_var)
  end; 
      
  
  writeln('');
  writeln('END')
end.