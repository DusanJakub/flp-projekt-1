:- consult(rules).
:- consult(window).

evaluate(infinity) :- myColor(C1), instantWin(C1), !.
evaluate(-infinity) :- otherColor(C2), instantWin(C2), !.

evaluate(E) :-
	myColor(C1), evalColor(C1,E1),
	otherColor(C2), evalColor(C2,E2),
	E is E1-E2,
	debugwrite("EVALUATE: %w = %w - %w\n", [E, E1, E2]).
	


evalColor(C,E) :-
	aggregate_all(count, stonesInRow(2,C,_), E2),
	aggregate_all(count, stonesInRow(3,C,_), E3),
	aggregate_all(count, stonesInRow(4,C,_), E4),
	aggregate_all(count, stonesInRow(5,C,_), E5),
	EE is E2/5 + E3/4 + 10*(E4/3 + E5/2),
	(colorToMove(C) -> E is EE+1 ; E is EE).


/*
evalColor(C,E) :-
	aggregate_all(count, threat(C,_), EE),
	(colorToMove(C) -> E is EE+1 ; E is EE).
*/	