module Parser (parsePascal, parseDouble, parseInteger) where
{--     
    Projekt:  FLP - Funkcionální projekt    
    Varianta: 03 - Interpret imperativního jazyka FLP-2014-Pascal  
    Team:     FLP 33
              Dusan Jakub     <xjakub19[at]stud.fit.vutbr.cz>
              Jakub Varadinek <xvarad01[at]stud.fit.vutbr.cz>
              Martin Brazdil  <xbrazd14[at]stud.fit.vutbr.cz>
              Marek Zak       <xzakma01[at]stud.fit.vutbr.cz>

    Popis:    Modul pro parser jazyka PASCAL upraveneho pro predmet FLP.
    Autori:   Dusan Jakub    <xjakub19[at]stud.fit.vutbr.cz>
              Martin Brazdil <xbrazd14[at]stud.fit.vutbr.cz>
    Datum:    12.3.2014
--}
import Types
import Text.Parsec 
import Text.Parsec.Char
import Text.Parsec.Expr
import qualified Text.Parsec.Token as P
import qualified Data.Map as Map 
import qualified Data.List as List
import qualified Control.Exception as E
import Data.Functor.Identity (Identity)

-- funkce pro provedeni parsovani (vstupni bod parseru)
parsePascal :: String -> Either ParseError Program
parsePascal  input = parse (programBody $ RootScope Map.empty) "structure" input

-- funkce provede nacteni cisla z kodu typu integer
parseInteger :: String -> Either ParseError Integer
parseInteger input = parse (do { whiteSpace; decimal }) "integer" input

-- funkce provede nacteni cisla z kodu typu double
parseDouble :: String -> Either ParseError Double
parseDouble  input = parse (do { whiteSpace; try float <|> (decimal >>= (return . (fromIntegral :: (Integral a) => a -> Double))) } ) "double" input

---------------------- Expression parser ---------------------

-- typ (monada) pro zaobeleni monady parseru
type Parser u a = ParsecT String u Identity a

-- funkce pro nacteni vyrazu (vytvari tabulku vyrazu)
expr :: Scope -> FunctionMap -> Parser u Expr
expr scope functions = buildExpressionParser table (term scope functions)
        <?>   "expression"

-- funkce pro nacteni vyrazu ve volani funkce (parametry) 
exprCallFunc :: Scope -> FunctionMap -> Parser u [Expr]
exprCallFunc scope functions = sepBy (expr scope functions) (symbol ",")

-- funkce pro nacteni termu (cisla, promenne, volani funkci)
term :: Scope -> FunctionMap -> Parser u Expr
term scope functions = parens (expr scope functions)
        <|> do 
              val <- try float
              whiteSpace
              return (DoubleConst val)
        <|> do 
              val <- decimal
              whiteSpace
              return (IntConst val)
        <|> do 
              val <- stringLiteral
              return (StringConst val)
        <|> do -- promenna nebo volani funkce -> nutne odlisit
              id <- identifier
              termId id scope functions
        <?> "simple expression" 

-- funkce pro nacteni (overeni) promenne nebo volani funkci
termId :: String -> Scope -> FunctionMap -> Parser u Expr
termId id scope functions = do -- volani funkce
              symbol "("
              exList <- exprCallFunc scope functions
              symbol ")"  
              -- overeni navazani vyrazu na parametry funkce (zvladne i pretizeni)
              case (checkAndConvertFunc id exList functions) of
                (Just exprs) -> do
                    let sig = funcSignatureStr id (exprsSignature exprs)
                    let func = case Types.lookupFuncMap sig functions of 
                                 (Just function) -> function
                                 otherwise  -> error $ ("Function " ++ sig ++ " is not declared.")
                    return $ Call func exprs
                otherwise -> fail $ ("Function " ++ (funcSignatureStr id (exprsSignature exList)) ++ " is not declared.")
        <|> do -- identifikator (promenna)
              case Types.lookupVar id scope of 
                (Just var) -> return (VarExpr var)
                otherwise  -> fail ("Variable " ++ id ++ " not declared.")

-- funkce pro ziskani / vytvoreni tabulky vyrazu s prioritami (precedencni tabulka)
table :: [[Operator String u Identity Expr]]
table = [ [binary "*" (Mult) AssocLeft, binary "div" (Div) AssocLeft ]
        , [binary "+" (Add)  AssocLeft, binary "-"   (Sub) AssocLeft ]
        , [  binary "<"  (Compare Types.LT) AssocLeft, binary "<=" (Compare Types.LE) AssocLeft
          ,  binary ">"  (Compare Types.GT) AssocLeft, binary ">=" (Compare Types.GE) AssocLeft
          ,  binary "<>" (Compare Types.NE) AssocLeft, binary "="  (Compare Types.EQ) AssocLeft ]
        ]

-- vytvoreni, respektive overeni, binarnich vyrazu 
binary :: String -> (Expr -> Expr -> Expr) -> Assoc -> Operator String u Identity Expr
binary name fun assoc = Infix ( do
    if (name == "div")
      then reserved name
      else reservedOp name
    return (\a b -> checkAndConvertExpr (fun a b)) 
  ) assoc

-- overeni a pripadne pretypovani parametru u vyrazu
checkAndConvertExpr :: Expr -> Expr
checkAndConvertExpr e@(Compare x a b) = 
  case (exprType a, exprType b) of
    (DoubleType, IntType)    -> Compare x a (IntToDouble b)
    (IntType, DoubleType)    -> Compare x (IntToDouble a) b
    (ta, tb) | ta == tb      -> e
    (ta, tb)                 -> error $ "Invalid combination of types for comparison: " ++ (show ta) ++ " and " ++ (show tb)

checkAndConvertExpr e@(Mult a b) = 
  case (exprType a, exprType b) of
    (DoubleType, IntType)    -> Mult a (IntToDouble b)
    (IntType, DoubleType)    -> Mult (IntToDouble a) b
    (IntType, IntType)       -> e
    (DoubleType, DoubleType) -> e
    (ta, tb)                 -> error $ "Invalid combination of types for multiplication: " ++ (show ta) ++ " and " ++ (show tb)

checkAndConvertExpr e@(Add a b) = 
  case (exprType a, exprType b) of
    (DoubleType, IntType)    -> Add a (IntToDouble b)
    (IntType, DoubleType)    -> Add (IntToDouble a) b
    (IntType, IntType)       -> e
    (DoubleType, DoubleType) -> e
    (StringType, StringType) -> e
    (ta, tb)                 -> error $ "Invalid combination of types for addition: " ++ (show ta) ++ " and " ++ (show tb)

checkAndConvertExpr e@(Sub a b) = 
  case (exprType a, exprType b) of
    (DoubleType, IntType)    -> Sub a (IntToDouble b)
    (IntType, DoubleType)    -> Sub (IntToDouble a) b
    (IntType, IntType)       -> e
    (DoubleType, DoubleType) -> e
    (ta, tb)                 -> error  $ "Invalid combination of types for subtraction: " ++ (show ta) ++ " and " ++ (show tb)

checkAndConvertExpr e@(Div a b) = 
  case (exprType a, exprType b) of
    (IntType, IntType)       -> e
    (ta, tb)                 -> error $ "Invalid combination of types for division: " ++ (show ta) ++ " and " ++ (show tb)
    
checkAndConvertExpr e = e

------------------ Overeni existence funkce dle nazvu a vyrazu (paramentru) pri volani ---------

-- nahrazeni n-teho prvku v seznamu
replaceNth :: (Eq a, Num a) => a -> b -> [b] -> [b]
replaceNth n newVal (x:xs)
     | n == 0 = newVal:xs
     | otherwise = x:(replaceNth (n-1) newVal xs)

-- overeni zda funkce se zadanym jmenem a typy parametru existuje
existFunc :: String -> [Expr] -> FunctionMap -> Bool
existFunc n exprs functions =
  case Types.lookupFuncMap (funcSignatureStr n (exprsSignature exprs)) functions of 
    (Just function) -> True
    otherwise  -> False

-- overeni, resp. konveverze (IntToDouble) vyrazu, na existenci funkce s danou signaturou
-- vraci upraveny seznam vyrazu
checkAndConvertFuncInner :: String -> [Expr] -> [Expr] -> FunctionMap -> Maybe [Expr]
checkAndConvertFuncInner n [] prevExprs functions =
  if (existFunc n (prevExprs) functions) == True 
    then Just prevExprs
    else Nothing

checkAndConvertFuncInner n (e:exprs) prevExprs functions = do
     let newE = if (exprType e) == IntType 
                  then (IntToDouble e)        -- konvrze
                  else e
     if (existFunc n (prevExprs ++ [newE] ++ exprs) functions) == True 
       then Just (prevExprs ++ [newE] ++ exprs)
       else checkAndConvertFuncInner n exprs (prevExprs ++ [e]) functions

-- overeni existence funkce se signaturou odpovidajici nazvu a dat. typum vyrazu
-- pro prirazeni do parametru
checkAndConvertFunc :: String -> [Expr] -> FunctionMap -> Maybe [Expr]
checkAndConvertFunc n exprs functions =
  case (existFunc n exprs functions) of
    True -> Just exprs  -- puvodni seznam vyrazu je ok -> exstuje funkce
    otherwise  -> do
      let sig = exprsSignature exprs
      let indexs = List.findIndices (=='I') sig
      case (checkAndConvertFuncInner n exprs [] functions) of
        (Just res) -> Just res
        otherwise  -> -- nenalezeno => prevod prvniho vyrazu typu int na double (jeho signatury)
          if (length indexs > 0)
            then do 
              let idx = indexs !! 0
              let newExprs = replaceNth idx (IntToDouble (exprs !! idx)) exprs
              checkAndConvertFunc n newExprs functions
            else Nothing

--------------------- Parser --------------------------------------

-- parsovani tela programu
programBody :: Scope -> Parser u Program
programBody parentScope = do
              whiteSpace
              -- parsovani glob. promennych
              globalScopeMap <- option Map.empty varBlock
              let globalScope = RootScope globalScopeMap
              -- parsovani deklaraci a definici funkci
              functions <- functionDecl globalScope Map.empty
              -- overeni zda jsou vsechny funkce definovane
              let res = filter (\n -> defined n == False) $ Map.elems functions
              if (length res) == 0 
                then return $ Program functions globalScope
                else fail $ "Functions "++ show (res >>= \n -> name n ++ ",") ++" are not defined."

-- parsovani deklaraci funkci
functionDecl :: Scope -> FunctionMap -> Parser u FunctionMap
functionDecl scope functions = do
              symbol "function"
              id <- identifier
              symbol "("
              scMap <- option Map.empty varFunc
              symbol ")"
              symbol ":"
              returnType <- dataType
              symbol ";"                
              let args = variables $ (LocalScope scope scMap)
              -- pridani nazev funkce do promennych              
              let localScopeMap = Map.insertWith (fail "Multiple declarations of the same variable") id returnType scMap
              
              -- dopredne pridani do seznamu funkci (kvuli self rekurzi)
              let tmp = Function id returnType args (LocalScope scope localScopeMap) EmptyStmt False
              let functionsTmp = case verifyFuncDefAndArgs tmp functions of 
                                   (Right f)  -> Map.insert (funcSignature f) f functions
                                   x          -> functions  -- chyby se preskoci

              -- volani parsovani definice                                                    
              (scopeLocal,stmts) <- functionBody (LocalScope scope localScopeMap) functionsTmp
              -- kontrola validity funkce
              let def = (stmts /= EmptyStmt)
              let func = Function id returnType args scopeLocal stmts def
              let f = case verifyFuncDefAndArgs func functions of 
                        (Right tmp) -> tmp
                        (Left x)    -> error x 
              functionDecl scope (Map.insert (funcSignature f) f functions)
        <|> do 
              -- sem se dostaneme jen v main funkci (bez hlavicky) 
              reserved "begin"
              stmts <- statementStart scope functions
              reserved "end"
              symbol "."
              let func = Function "" EmptyType [] scope stmts True
              return $ Map.insert "" func functions

-- parsovani tela funkce (vc. lokalnich promennych)
functionBody :: Scope -> FunctionMap -> Parser u (Scope, Stmt)
functionBody scope functions = do
              varScMap <- option Map.empty varBlock 
              -- slouceni lokalnich promennych s argumenty funkce a navratove promenne
              let scMap = scopeMap scope
              let f key leftVal rightVal = error $ "Multiple declarations of the same variable "++(show key)
              let localScMap = Map.unionWithKey f scMap varScMap
              let localScope = LocalScope (scopeParent scope) localScMap
              -- telo funkce
              reserved "begin"
              stmts <- statementStart localScope functions 
              reserved "end"
              return (localScope, stmts)
        <|> do  
              -- funkce bez tela == deklarace funkce
              return (scope, EmptyStmt)
  
-- overeni na redeklaraci/redefinici jedne funkce
verifyFuncDefAndArgs :: Function -> FunctionMap -> Either String Function               
verifyFuncDefAndArgs func functions = do
  case Types.lookupFuncMap (funcSignature func) functions of 
       (Just funcOld) ->  
         if ( ((defined func) && (defined funcOld)) || (defined func == False))  
           then Left $ "Function " ++ (funcSignature func) ++" already defined or declared."  
           else case (funcSignature func) == (funcSignature funcOld) of 
                  True -> Right $ recreateFunction func
                  False -> Left $ "Function arguments of declaration "++ (funcSignature funcOld) ++" and definition "++ (funcSignature func) ++" are different."
       otherwise -> Right func

-- parsovani bloku promennych var
varBlock :: Parser u ScopeMap
varBlock  = do { reserved "var"; localScopeMap <- varDecls; symbol ";"; return localScopeMap }

-- obsah bloku promennych
varFunc :: Parser u ScopeMap
varFunc   = do { localScopeMap <- varDecls; return localScopeMap }

-- deklarace promennych
varDecls :: Parser u ScopeMap
varDecls  = do { vars <- sepBy varDecl (symbol ","); return $ Map.fromListWith (fail "Multiple declarations of the same variable") vars }

-- parsovani samotne deklarace promenne tvaru name : type
varDecl :: Parser u (String, DataType)
varDecl   = do { id <- identifier; symbol ":"; t <- dataType; return (id, t) }

-- parsovani datoveho typu ve vstupnim zdrojovem souboru
dataType :: Parser u DataType
dataType  = do { reserved "integer"; return IntType }
        <|> do { reserved "double";  return DoubleType }
        <|> do { reserved "string";  return StringType }
         
-- parsovani prikazu - presneji obsahu hlanvnich bloku funkci a programu
-- tedy telo mezi definicnimy bloky begin a end
statementStart :: Scope -> FunctionMap -> Parser u Stmt          
statementStart scope functions = do 
              stmts <- sepBy (statement scope functions) (symbol ";")
              whiteSpace
              return $ SeqStmt stmts

-- parsovani prikazu (blok begin end, prirazeni, if then else, while do, 
-- readln, writeln)
statement :: Scope -> FunctionMap -> Parser u Stmt
statement scope functions = do 
              reserved "begin"
              stmts <- sepBy (statement scope functions) (symbol ";")
              whiteSpace
              reserved "end"
              return $ SeqStmt stmts
        <|> do 
              id <- identifier
              symbol ":="
              ex <- expr scope functions; 
              case Types.lookupVar id scope of 
                (Just var) -> return $ checkAndConvertStmt (AssignStmt var ex) 
                otherwise  -> fail ("Variable " ++ id ++ " is not declared.")
        <|> do 
              reserved "if"
              cond <- expr scope functions
              reserved "then"
              pos <- statement scope functions
              whiteSpace
              reserved "else"
              neg <- statement scope functions
              if (exprType cond == BooleanType)
                then return $ CondStmt cond pos neg
                else fail $ "If condition must be boolean."
        <|> do 
              reserved "while"
              cond <- expr scope functions
              reserved "do"
              loop <- statement scope functions
              if (exprType cond == BooleanType)
                then return $ WhileStmt cond loop
                else fail $ "While condition must be boolean."
        <|> do
              reserved "readln"
              symbol "("
              id <- identifier
              symbol ")"
              case Types.lookupVar id scope of 
                (Just var) -> return $ ReadStmt var 
                otherwise  -> fail ("Variable " ++ id ++ " is not declared.")
        <|> do
              reserved "writeln"
              symbol "("
              ex <- expr scope functions
              symbol ")"
              return $ WriteStmt ex

-- overeni a uprava (konverze IntToDouble) prikazu (prirazovaci prikaz)
checkAndConvertStmt :: Stmt -> Stmt
checkAndConvertStmt s@(AssignStmt var ex) = 
  case (varType var, exprType ex) of 
    (DoubleType, IntType) -> AssignStmt var (IntToDouble ex)
    (ta, tb) | ta == tb   -> s
    (ta, tb)              -> error $ "Cannot assign expresion of type " ++ (show tb) ++ " to variable " ++ (varName var) ++ " of type " ++ (show ta)

--------------------- Lexer --------------------------------------

-- definice "konstant" pro lexer
langDef  :: P.LanguageDef st
langDef   = P.LanguageDef
    { P.commentStart   = "{"
    , P.commentEnd     = "}"
    , P.commentLine    = ""
    , P.nestedComments = False
    , P.identStart     = letter   <|> char '_'
    , P.identLetter    = alphaNum <|> char '_'
    , P.reservedNames  = ["begin", "div", "do", "double", "else", "end", 
                          "if", "integer", "readln", "string", "then", 
                          "var", "while", "writeln"]
    , P.reservedOpNames= ["*", "+", "-", "<", "<=", "=", "<>", ">", ">="]
    , P.caseSensitive  = True
    , P.opStart        = oneOf "*+-<=>"
    , P.opLetter       = oneOf "=>"
    }

-- aliasy pro knihovni funkce parseru
lexer       = P.makeTokenParser langDef    
parens      = P.parens lexer
braces      = P.braces lexer
identifier  = P.identifier lexer
reserved    = P.reserved lexer
reservedOp  = P.reservedOp lexer
decimal     = P.decimal lexer
float       = P.float lexer
lexeme      = P.lexeme lexer
symbol      = P.symbol lexer
whiteSpace  = P.whiteSpace lexer

-- funkce pro parsovani retezcoveho literalu
stringLiteral :: Parser u String
stringLiteral = lexeme (do
            str <- between (char '\'') (char '\'' <?> "end of string") (many stringChar)
            return (foldr (maybe id (:)) "" str)
        <?> "literal string")

-- funkce pro ziskani znaku v retezcovem literalu
stringChar :: Parser u (Maybe Char)
stringChar    =   do { c <- stringLetter; return (Just c) }
        <|> do { try (string "''"); return (Just '\'') }
        <?> "string character"

-- funkce pro ziskani pismena v retezcovem literalu
stringLetter :: Parser u Char
stringLetter  = satisfy (\c -> (c /= '\'') && (c > '\026'))
