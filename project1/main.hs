{-- 
    Projekt:  FLP - Funkcionální projekt    
    Varianta: 03 - Interpret imperativního jazyka FLP-2014-Pascal  
    Team:     FLP 33
              Dusan Jakub     <xjakub19[at]stud.fit.vutbr.cz>
              Jakub Varadinek <xvarad01[at]stud.fit.vutbr.cz>
              Martin Brazdil  <xbrazd14[at]stud.fit.vutbr.cz>
              Marek Zak       <xzakma01[at]stud.fit.vutbr.cz>

    Popis:    Hlavni soubor pro ovladani interpretu jazyka Pascal.
    Autori:   Dusan Jakub     <xjakub19[at]stud.fit.vutbr.cz>
    Datum:    12.3.2014
--}
import Parser
import Interpreter
import Types
import System.IO
import System.Environment
import System.Exit

-- hlavni funkce pro vykani parsovani a interpretovani kodu v Pascalu
main :: IO ()
main = do  
	-- nacteni vstupnich parametru
	args <- getArgs 
	file <- case args of
		[file] -> return file
		otherwise -> do
						name <- getProgName
						hPutStrLn stderr $ "Usage: " ++ name ++ " <string>"
						exitFailure
    -- nacteni obsahu vstupniho souboru
	inh <- openFile file ReadMode
	contents <- hGetContents inh 
    -- parsovani a interpretovani kodu
	case parsePascal contents of
		(Right program) -> do { res <- interpretPascal program; putStr "" {-- print res --} }
		x -> hPrint stderr x
    -- uzavreni vstupniho souboru
	hClose inh 
