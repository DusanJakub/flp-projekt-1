var x:integer, res:integer;

begin
  
  writeln('x = 1 + 2 * 3');
  x := 1+2*3;
  res := 7;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ');
  
  
  
  writeln('x = 1 * 2 + 3');
  x     :=     1*   2+  3;
  res := 5;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ;
  

  
  writeln('x = 1 * (2 + 3)');
  x     :=     1*   (2+  3);
  res := 5;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ;
  
  
  
  writeln('x = 4 * (2 + 3)');
  x     :=     4*   (2+  3);
  res := 20;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ;
  
  
    
  writeln('x = (1 + 2) * 3');
  x     :=     (1+   2)*  3;
  res := 9;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ;
  
  
  
  writeln('x = (4 * 2 + 3)');
  x     :=   (  4*   2+  3);
  res := 11;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ; 
  
  
  
  writeln('x = (5+1) * (2 + 3)');
  x     :=     (5+1)  *   (2+  3);
  res := 30;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ;
    
  
  
  writeln('x = (5*1) + (2 * 3)');
  x     :=     (5*1)   +     (2*  3);
  res := 11;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') ;
    
  
  
  writeln('x = (5+4 * 2 + 3)');
  x     :=     (5+4*   2+  3);
  res := 16;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ');
    
  
  
  writeln('x = (5div1) * (2div 3)');
  x     :=     (5div1)*   (2div 3);
  res := 0;
  writeln(x);
  
  if   x   <>   res   then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' '); 
  
  
    writeln('x = (5)*(2)');
  x     :=     (5)*(2);
  res := 10;
  writeln(x);
  
  if   x   <>   res   then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' '); 
  
  
  
    writeln('x = (5div1) + (2div 3)');
  x     :=     (5div1)+   (2div 3);
  res := 5;
  writeln(x);
  
  if x <> res then
    writeln('WRONG')
  else
    writeln('OK');
  writeln(' ') 
      
  
end.